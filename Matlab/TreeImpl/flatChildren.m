function flatChildren(root, parent, pJacobian)

for i = 1:parent.getChldrnNumber()
    child = parent.getChild(i);
    
    augJac = [pJacobian, zeros(size(pJacobian, 1), child.k)];
    combJac = [augJac; child.A];
    
    if (child.isLeaf)
        
        if isfield(child.options, 'calcUnfoc')
            options.calcUnfoc = child.options.calcUnfoc;
        else
            options.calcUnfoc = false;
        end

        if isfield(child.options, 'calcFoc')
            options.calcFoc = child.options.calcFoc;
        else
            options.calcFoc = false;
        end

        if isfield(child.options, 'focIndeces')
            options.focIndeces = child.options.focIndeces;
        else
            options.focIndeces = [];
        end

        if isfield(child.options, 'condCovMode')
            options.condCovMode = child.options.condCovMode;
        end

        options.name = [child.options.name, ' - copy'];
        if (root.N == size(combJac, 2))
            NAugAction(root, combJac, options);
        else
            RectAction(root, combJac, options);
        end
    else
        flatChildren(root, child, combJac);
    end
end


end

