clear all;
close all;

rng(100);

addpath(genpath('invChol'));
t_vec1 = [];
t_vec2 = [];
t_vec3 = [];
t_vec4 = [];

for i = 10:10
    % joint state
    A_all = 0.1*rand(10*i,i);

    % determine reordering so that eigenvalues appear sorted
    [~, ~,P]=qr(A_all);
    A_all = A_all*P;
    M = A_all'*A_all;

    A = 0.1*rand(ceil(i/2),i);

    dim = size(M, 1);
    t1 = 0;
    t2 = 0;
    t3 = 0;
    t4 = 0;

    [M_inv, t1] = calcSymmInverse(M);
    tic
    V1 = A * M_inv * A';
    t1 = t1 + toc;
    t_vec1 = [t_vec1 t1];

    tic
    RR = chol(M);
    VV = RR'\A';
    V2 = VV'*VV;
    t2 = t2 + toc;
    t_vec2 = [t_vec2 t2];

    I=eye(dim);

    tic
    RR = chol(M);
    VV = RR\(RR'\A');
    V3 = A*VV;
    t3 = t3 + toc;
    t_vec3 = [t_vec3 t3];
    
    
%     huptriang = dsp.UpperTriangularSolver;
% 
%     tic
%     RR = chol(M);
%     VV1 = huptriang(RR', A')
%     V4 = VV1'*VV1;
%     t4 = t4 + toc;
    t_vec4 = [t_vec4 t4];
end

indeces = 1:(length(t_vec1));
plot(indeces, t_vec1, indeces, t_vec2, indeces, t_vec3, indeces, t_vec4);
legend('inverse', 'bracket', 'double bracket', 'bracket mex');