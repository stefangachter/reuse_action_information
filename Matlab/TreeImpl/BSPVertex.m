
classdef BSPVertex < Parent
    
    properties
        calcIndeces
        calcCov
        calcCondIndeces
        calcCondCov
        timing
        reuse
    end
    
    methods

      function obj = BSPVertex()
         obj = obj@Parent();
         obj.calcIndeces = [];
         obj.calcCov = [];
      end

      function varCov = getCov(obj, varIndeces)
          
          varCov = [];
          
          if (isempty(obj.calcIndeces) || isempty(obj.calcCov))
              warning('Covarience entries were not calculated yet.')
              return;
          end;

          if (~isempty(setdiff(varIndeces, obj.calcIndeces)))
              warning('The required conditional covarience entries were not calculated.')
              return;
          end

          idxs = arrayfun(@(x)find(obj.calcIndeces==x,1),varIndeces);
          varCov = obj.calcCov(idxs, idxs);
      end

      function varCov = getCondCov(obj, varIndeces)
          
          varCov = [];
          
          if (isempty(obj.calcCondIndeces) || isempty(obj.calcCondCov))
              warning('Conditional covarience entries were not calculated yet.')
              return;
          end;
          
          if (~isempty(setdiff(varIndeces, obj.calcCondIndeces)))
              warning('The required conditional covarience entries were not calculated.')
              return;
          end
          
          idxs = arrayfun(@(x)find(obj.calcCondIndeces==x,1),varIndeces);
          varCov = obj.calcCondCov(idxs, idxs);
      end

      function t = getCompTime(obj)
          t = 0;
          
          if isfield(obj.timing, 'cov')
              t = t + obj.timing.cov;
          end

          if isfield(obj.timing, 'cond_cov')
              t = t + obj.timing.cond_cov;
          end

          if isfield(obj.timing, 'infrmtn')
              t = t + obj.timing.infrmtn;
          end
      end
    end

    methods (Abstract)
      getName(obj)
    end

end

