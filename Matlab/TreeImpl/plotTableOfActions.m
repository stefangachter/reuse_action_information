function [] = plotTableOfActions(root)
% Plot table with details of all actions

CLR_RED = '#FF0000';
CLR_GREEN = '#00FF00';
CLR_DARK_GREEN = '#006400';
CLR_BLUE = '#0000FF';
CLR_BLACK = '#000000';
CLR_WHITE = '#FFFFFF';

tbl = uitable;
colNum = 22;

% Create initial data for table
data = cell(0,colNum);
data = getTableDataOfTree(root, data);
actNum = size(data,1);

% indeces
unfoc_inf_ind = 13;
sum_foc_inf_ind = unfoc_inf_ind + 3;
cov_time_ind = 17;
cond_cov_time_ind = cov_time_ind + 1;
inf_time_ind = cond_cov_time_ind + 1;
all_time_ind = inf_time_ind + 1;

% Format cov time
covTimeStrs = cell(actNum, 1);
covTimeSum = 0;
for i = 1:actNum
    covTime = data{i,cov_time_ind};
    
    if (~isempty(covTime))
        covTimeStrs{i,1} = num2str(covTime, '%.10f');
        covTimeSum = covTimeSum + covTime;
    end
end
data(:,cov_time_ind) = covTimeStrs;

% Format cond_cov time
condCovTimeStrs = cell(actNum, 1);
condCovTimeSum = 0;
for i = 1:actNum
    condCovTime = data{i,cond_cov_time_ind};
    
    if (~isempty(condCovTime))
        condCovTimeStrs{i,1} = num2str(condCovTime, '%.10f');
        condCovTimeSum = condCovTimeSum + condCovTime;
    end
end
data(:,cond_cov_time_ind) = condCovTimeStrs;

% Format inf time
infTimeStrs = cell(actNum, 1);
infTimeSum = 0;
for i = 1:actNum
    infTime = data{i,inf_time_ind};
    
    if (~isempty(infTime))
        infTimeStrs{i,1} = num2str(infTime, '%.10f');
        infTimeSum = infTimeSum + infTime;
    end
end
data(:,inf_time_ind) = infTimeStrs;

% Adding summary row
sumRow = cell(1,colNum);
sumRow{1} = 'Summary';
sumRow{cov_time_ind} = num2str(covTimeSum, '%.10f');
sumRow{cond_cov_time_ind} = num2str(condCovTimeSum, '%.10f');
sumRow{inf_time_ind} = num2str(infTimeSum, '%.10f');
sumRow{all_time_ind} = num2str(covTimeSum+condCovTimeSum+infTimeSum, '%.10f');
data = [data; sumRow];

% Color table cells
colorMat = cell(size(data));
bcolorMat = cell(size(data));

colorMat(1:(end-1),unfoc_inf_ind:sum_foc_inf_ind) = {CLR_BLUE};
colorMat(1:(end-1),cov_time_ind:all_time_ind) = {CLR_DARK_GREEN};

% Color current belief row
bcolorMat(1, :) = {'#E9967A'};

% Color "Leaf" rows
for i = 2:actNum
    if (data{i,3})
        bcolorMat(i, :) = {'#00CED1'};
    else
        bcolorMat(i, :) = {CLR_WHITE};
    end
end

% Color summary row
colorMat(end, :) = {CLR_GREEN};
bcolorMat(end, :) = {CLR_BLACK};

data = colorData(data, colorMat, bcolorMat);

% Update UI table
tbl.Data = data;
margin = 0.05;
set(tbl, 'Units', 'normalized', 'Position', [margin,margin,1-2*margin,1-2*margin]);
tbl.ColumnName = {'Name', 'Type', 'Leaf', 'Parent','n','k','m', 'inv_D','Unfoc','Foc',...
                  'Foc Indeces', 'Foc_D', 'Unfoc Inf',...
                  'Unfoc Inf - Sum', 'Foc Inf', 'Foc Inf - Sum',...
                  'Cov Calc Time', 'Cond Cov Calc Time', 'Inf Calc Time', 'All Time'};
tbl.ColumnEditable = true;
tbl.ColumnWidth = {130,70,40,130,50,50,50,50,40,40,150,40,80,100,80,80,100,120,100,100,80,80,80};

end

