clear all;
close all;

rng(100);

addpath(genpath('invChol'));
t_vec1 = [];
t_vec2 = [];
t_vec3 = [];
t_vec4 = [];

for i = 500:100:2000
    % joint state
    A_all = 0.1*rand(10*i,i);

    % determine reordering so that eigenvalues appear sorted
    [~, ~,P]=qr(A_all);
    A_all = A_all*P;
    M = A_all'*A_all;

    dim = size(M, 1);
    t1 = 0;
    t2 = 0;
    t3 = 0;
    t4 = 0;

    tic
    RR = chol(M);
    t1 = t1 + toc;
    t_vec1 = [t_vec1 t1];

    tic
    RR = chol_mex(M);
    t2 = t2 + toc;
    t_vec2 = [t_vec2 t2];

    t_vec3 = [t_vec3 t3];
    
    t_vec4 = [t_vec4 t4];
end

indeces = 1:(length(t_vec1));
plot(indeces, t_vec1, indeces, t_vec2, indeces, t_vec3, indeces, t_vec4);
legend('chol', 'chol mex', 'double bracket', 'bracket mex');