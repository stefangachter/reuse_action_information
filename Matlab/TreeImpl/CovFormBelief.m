
classdef CovFormBelief < BasicBelief
    
    properties
        S
    end
    
    methods
      function obj = CovFormBelief(S, options)
          options.null = true;
          obj = obj@BasicBelief(options);
          
          obj.S = S;
          obj.N = size(S, 1);
      end

      function [] = computeCov(obj)
          t = 0;

          obj.calcCov = obj.S(obj.calcIndeces, obj.calcIndeces);
          obj.timing.cov = t;
      end

      function [] = computeCondCov(obj)
          t = 0;
          
          % Calculate conditional covariance
          UF_cov = obj.S([obj.calcCondIndeces, obj.options.focIndeces], [obj.calcCondIndeces, obj.options.focIndeces]);
          inn = 1:length(obj.calcCondIndeces);
          foc = [1:length(obj.options.focIndeces)] + max(inn);
          F_marg_cov = UF_cov(foc,foc);
          U_marg_cov = UF_cov(inn,inn);
          UF_marg_cov = UF_cov(inn,foc);

          [R, t1] = calcQForm(F_marg_cov, UF_marg_cov);
          t = t + t1;

          tic
          cov_cond = U_marg_cov - R'*R;
          t = t + toc;

          obj.calcCondCov = cov_cond;
          obj.timing.cond_cov = t;
      end

    end
    
end

