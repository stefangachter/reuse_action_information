
classdef GtsamBelief < BasicBelief
    
    properties
        graph
        values
        foc_dim
        unfocIndeces
        unfoc_dim
        L_cond
    end
    
    methods
      function obj = GtsamBelief(graph, values, options)
          options.null = true;
          obj = obj@BasicBelief(options);
          
          obj.graph = graph;
          obj.values = values;
          obj.N = double(values.dim);
      end

      function [priorIndeces, priorCondIndeces] = checkReqPriorCovIndeces(obj)
          [priorIndeces, priorCondIndeces] = obj.checkReqPriorCovIndeces@BasicBelief();
          
          if (~isempty(priorCondIndeces))              
              if (~isfield(obj.options, 'focIndeces') || isempty(obj.options.focIndeces))
                  warning('No variables are focused in the information-form belief!!!');
                  return;
              end
              
              obj.foc_dim = length(obj.options.focIndeces);
              obj.unfocIndeces = setdiff([1:obj.N], obj.options.focIndeces);
              obj.unfoc_dim = length(obj.unfocIndeces);

              if ~isfield(obj.options, 'condCovMode')
                  if (obj.foc_dim > obj.unfoc_dim)
                      obj.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
                  else
                      obj.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
                  end
              end

              if (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_MARG))
                  priorIndeces = union(priorIndeces, priorCondIndeces)';
                  priorIndeces = union(priorIndeces, obj.options.focIndeces)';
              elseif (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_INFP))
                  % Do Nothing
              end
          end
      end

      function [] = computeCov(obj)
          t = 0;
          
          priorKeys = obj.values.getIndVars(obj.calcIndeces);

          
          
%           gfg = obj.graph.linearize(obj.values.getGTSamValues());
%           iii = gfg.hessian();
%           ccc = inv(iii);
%           cond_iii_orig = iii(obj.calcIndeces, obj.calcIndeces);
%           
%           all_keys = gtsam.KeyVector(obj.values.getGTSamValues().keys());
%           
%           unfocKeys = obj.values.getIndVars(obj.unfocIndeces);
%           [cond_d,~] = gfg.eliminatePartialMultifrontal(unfocKeys);
%           cond_gfg = gtsam.GaussianFactorGraph(cond_d);
%           ordering = SetOrderingFromKeyVector(unfocKeys);    
%           cond_A = cond_gfg.jacobian(ordering);
%           indSize = length(obj.calcIndeces);
%           cond_A = cond_A(1:indSize, 1:indSize);
%           obj.L_cond = cond_A'*cond_A;
%           
%           
%           
%           errr = max(max(abs(cond_iii_orig - cond_iii)));
%           if (errr > 0.0000001)
%               'bad'
%           end

          
          
          
          
          
          
          
          
          
          marginals = gtsam.Marginals(obj.graph, obj.values.getGTSamValues());
          tic;
          cov = marginals.jointMarginalCovariance(priorKeys).fullMatrix();
          t = t + toc;

          obj.calcCov = cov;          
          obj.timing.cov = t;
      end

      function [] = computeCondCov(obj)
          t = 0;

          % Calculate conditional covariance
          if (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_MARG))
              UF_cov = obj.getCov([obj.calcCondIndeces, obj.options.focIndeces]);
              inn = 1:length(obj.calcCondIndeces);
              foc = [1:length(obj.options.focIndeces)] + max(inn);
              F_marg_cov = UF_cov(foc,foc);
              U_marg_cov = UF_cov(inn,inn);
              UF_marg_cov = UF_cov(inn,foc);

              [R, t1] = calcQForm(F_marg_cov, UF_marg_cov);
              t = t + t1;

              tic
              cov_cond = U_marg_cov - R'*R;
              t = t + toc;

          elseif (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_INFP))

              unfocKeys = obj.values.getIndVars(obj.unfocIndeces);
              gfg = obj.graph.linearize(obj.values.getGTSamValues());
              ordering = SetOrderingFromKeyVector(unfocKeys);    
              
              tic;
              [cond_d,~] = gfg.eliminatePartialMultifrontal(unfocKeys);
              cond_gfg = gtsam.GaussianFactorGraph(cond_d);
              cond_A = cond_gfg.jacobian(ordering);
              cond_A = cond_A(:, 1:obj.unfoc_dim);
              obj.L_cond = cond_A'*cond_A;
              t = t + toc;
              
              I_N = eye(obj.unfoc_dim);
              idxs = arrayfun(@(x)find(obj.unfocIndeces==x,1),obj.calcCondIndeces);
              
              tic;
              P = obj.L_cond \ I_N(:,idxs);
              cov_cond = P(idxs,:);
              t = t + toc;
              
              
              
              
              
              
              
              
              
%               iii = gfg.hessian();
%               ccc = inv(iii);
%               UF_cov = ccc([obj.calcCondIndeces, obj.options.focIndeces],[obj.calcCondIndeces, obj.options.focIndeces]);
%               inn = 1:length(obj.calcCondIndeces);
%               foc = [1:length(obj.options.focIndeces)] + max(inn);
%               F_marg_cov = UF_cov(foc,foc);
%               U_marg_cov = UF_cov(inn,inn);
%               UF_marg_cov = UF_cov(inn,foc);
%               [R, ~] = calcQForm(F_marg_cov, UF_marg_cov);
%               cov_cond1 = U_marg_cov - R'*R;
% 
%               errr = max(max(abs(cov_cond - cov_cond1)));
%               if (errr > 0.0000001)
%                   'bad'
%               end
              
              
              
          end

          obj.calcCondCov = cov_cond;
          obj.timing.cond_cov = t;
      end

    end
    
end

