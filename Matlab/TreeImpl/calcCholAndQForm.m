function [V, R, t] = calcCholAndQForm(M, A)
% 1) Computes Cholesky of M
% Returns matrix V:
% V' * V = M
% 2) Solves A * inv(M) * A'
% Returns matrix R:
% R' * R = A * inv(M) * A'

tic
V = chol(M);
R = V'\A';
t = toc;
    
end

