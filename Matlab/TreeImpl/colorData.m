function [data] = colorData(data, colorMat, bcolorMat)
% Color table cells within uitable

for i = 1:size(data,1)
    for j = 1:size(data,2)
        currData = data{i,j};
        color = colorMat{i,j};
        bcolor = bcolorMat{i,j};
        
        if (isempty(currData))
            data{i,j} = colorText(bcolor,bcolor,'&nbsp;');
        elseif (islogical(currData))
            data{i,j} = colorCheckbox(bcolor,currData);
        else
            data{i,j} = colorText(color,bcolor,currData);
        end
    end
end

end

