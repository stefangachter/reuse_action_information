function agregateInfToLeafs(node, unfocInf, focInf)

if (node.options.calcUnfoc)
    if (isnan(unfocInf))
        unfocInf = node.unfocInfrmtn;
    else
        unfocInf = unfocInf + node.unfocInfrmtn;
    end
end

if (node.options.calcFoc)
    if (~isfield(focInf, 'focIndeces'))
        focInf.i = node.focInfrmtn;
        focInf.focIndeces = node.options.focIndeces;
    else
        if (numel(setxor(focInf.focIndeces,node.options.focIndeces)) == 0)
            focInf.i = focInf.i + node.focInfrmtn;
        else
            focInf.i = node.focInfrmtn;
            focInf.focIndeces = node.options.focIndeces;
        end
    end    
end

if (node.isLeaf())
    node.sumUnfocInfrmtn = unfocInf;
    
    if (~isfield(focInf, 'focIndeces'))
        node.sumFocInfrmtn = NaN;
    else
        node.sumFocInfrmtn = focInf.i;
    end
else
    for i = 1:node.getChldrnNumber()
        child = node.getChild(i);
        agregateInfToLeafs(child, unfocInf, focInf);
    end
end

end

