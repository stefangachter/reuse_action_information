function [u] = setsUnion(s1, s2)
% Unite two sets

u = union(s1, s2);
u = u(:)';

end

