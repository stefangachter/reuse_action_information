function [R, t] = calcQForm(M, A, V)
% Solves A * inv(M) * A'
% Returns matrix R:
% R' * R = A * inv(M) * A'

if (nargin == 2)
    tic
    V = chol(M);
    R = V'\A';
    t = toc;
elseif (nargin == 3)
    tic
    R = V'\A';
    t = toc;
else
    R = [];
    t = 0;
end
    
end

