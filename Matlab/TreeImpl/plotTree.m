function [] = plotTree(root, plotDetails)

[arr, nodes] = buildTreeArray([], {}, 0, root);

%treeplot(arr);
xlim([0 1]);
ylim([0 1]);
hold on;
[x,y] = treelayout(arr);
xy = [x',y'];
xy(:,1) = xy(:,1) - min(x) + 0.1;
x_perm = rand(length(arr),1);
x_perm = (x_perm - 0.5)/60;
xy(:,1) = xy(:,1) + x_perm;

% Plot edges
for i=1:length(x)
    p_ind = arr(i);
    if (p_ind ~= 0)
        plot([xy(p_ind,1), xy(i,1)],[xy(p_ind,2), xy(i,2)], 'red');
    end
end

% Plot verteces
for i=1:length(x)
    node = nodes{i};

    plot(xy(i,1), xy(i,2), 'o', 'MarkerSize', 7,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor',node.getColor());
    
    newx = xy(i,1)+0.01;
    newy = xy(i,2);
    t = text(newx, newy,node.getName());
    newy = t.Extent(2)-t.Extent(4);
    

    if (~plotDetails ||...
        ~isprop(node, 'options'))
        continue;
    end
    
    if (isfield(node.options, 'calcUnfoc') && node.options.calcUnfoc && isprop(node, 'unfocInfrmtn'))
        strRes = ['Unfoc. Inf.: ', num2str(node.unfocInfrmtn)];
        t = text(newx,newy,strRes, 'Color', 'blue');
        newy = t.Extent(2)-t.Extent(4);
    end

    if (isfield(node.options, 'calcFoc') && node.options.calcFoc && isprop(node, 'focInfrmtn'))
        strRes = ['Foc. Inf.: ', num2str(node.focInfrmtn)];
        t = text(newx,newy,strRes, 'Color', 'blue');
        newy = t.Extent(2)-t.Extent(4);
    end

    if isfield(node.timing, 'cov')
        strRes = ['Cov. Calc.: ', num2str(node.timing.cov), ' s'];
        t = text(newx,newy,strRes, 'Color', 'green');
        newy = t.Extent(2)-t.Extent(4);
    end

    if isfield(node.timing, 'cond_cov')
        strRes = ['Cond. Cov. Calc.: ', num2str(node.timing.cond_cov), ' s'];
        t = text(newx,newy,strRes, 'Color', 'green');
        newy = t.Extent(2)-t.Extent(4);
    end

    if isfield(node.timing, 'infrmtn')
        strRes = ['Inform. Calc.: ', num2str(node.timing.infrmtn), ' s'];
        t = text(newx,newy,strRes, 'Color', 'green');
        newy = t.Extent(2)-t.Extent(4);
    end

    if (node.isLeaf())
        newy = newy + (t.Extent(4)/2);
        plot([newx, newx + t.Extent(3)],[newy, newy], 'red');
        newy = newy - t.Extent(4);
    end
    
    if (node.isLeaf() && ~isnan(node.sumUnfocInfrmtn))
        strRes = ['Unfoc. Inf. Aggr.: ', num2str(node.sumUnfocInfrmtn)];
        t = text(newx,newy,strRes, 'Color', 'blue');
        newy = t.Extent(2)-t.Extent(4);
    end
    
    if (node.isLeaf() && ~isnan(node.sumFocInfrmtn))
        strRes = ['Foc. Inf. Aggr.: ', num2str(node.sumFocInfrmtn)];
        t = text(newx,newy,strRes, 'Color', 'blue');
        newy = t.Extent(2)-t.Extent(4);
    end

end

newx = 0.01;
newy = 1-t.Extent(4);
tim = calcTimeOfTree(root);
strRes = ['Required Time: ', num2str(tim), ' s'];
t = text(newx,newy,strRes, 'Color', 'green');
%newy = t.Extent(2)-t.Extent(4);

end

