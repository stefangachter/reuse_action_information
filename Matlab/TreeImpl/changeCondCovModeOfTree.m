function [] = changeCondCovModeOfTree(root, condCovMode)
% Change condCovMode of all nodes

root.options.condCovMode = condCovMode;

for i = 1:root.getChldrnNumber()
    child = root.getChild(i);
    
    changeCondCovModeOfTree(child, condCovMode);
end

end

