function [u] = setsUnionMultiple(sets)
% Unite multiple sets

setsNum = length(sets);

u = sets{1};
u = u(:)';

for i = 2:setsNum
    u = union(u, sets{i});
    u = u(:)';
end

end

