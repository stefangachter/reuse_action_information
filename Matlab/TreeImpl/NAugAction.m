classdef NAugAction < BasicAction
    
    properties
    end
    
    methods
      function obj = NAugAction(P, A, options)
          options.null = true;
          obj = obj@BasicAction(P, A, options);
          
          if (obj.k ~= 0)
              error('The provided Jacobian is not appropriate for Not Augmented Action');
          end
      end
      
      function calcPosteriorCov(obj)
          t = 0;
          S_prior = obj.parent.getCov([obj.invVars, obj.Vold]);
          S_I = S_prior(1:length(obj.invVars), 1:length(obj.invVars));
          S_V = S_prior(length(obj.invVars)+1:end, length(obj.invVars)+1:end);
          S_C = S_prior(length(obj.invVars)+1:end, 1:length(obj.invVars));
          I_m = eye(obj.m);

          tic
          Cmat = I_m + obj.I_old * S_I * obj.I_old';
          P = S_C * obj.I_old';
          t = t + toc;

          [Cmat_chol, F, t1] = calcCholAndQForm(Cmat, P);
          t = t + t1;

          % Keep it - it may be required to calculate the information
          obj.reuse.Cmat_chol = Cmat_chol;

          tic
          obj.calcCov = S_V - F'*F;
          t = t + toc;
          
          obj.timing.cov = t;
      end

      function calcPosteriorCondCovThroughPropagation(obj)
          t = 0;
          S_prior = obj.parent.getCondCov([obj.invUVars, obj.cond_Vold]);
          S_I = S_prior(1:length(obj.invUVars), 1:length(obj.invUVars));
          S_V = S_prior(length(obj.invUVars)+1:end, length(obj.invUVars)+1:end);
          S_C = S_prior(length(obj.invUVars)+1:end, 1:length(obj.invUVars));
          I_m = eye(obj.m);

          tic
          Cmat = I_m + obj.I_U_old * S_I * obj.I_U_old';
          P = S_C * obj.I_U_old';
          t = t + toc;

          [Cmat_chol, F, t1] = calcCholAndQForm(Cmat, P);
          t = t + t1;

          % Keep it - it may be required to calculate the information
          obj.reuse.Cmat_chol_cond = Cmat_chol;

          tic
          obj.calcCondCov = S_V - F'*F;
          t = t + toc;
          
          obj.timing.cond_cov = t;
      end

      function calcUnfocusedInformation(obj)
          t = 0;
          
          S_I = obj.parent.getCov(obj.invVars);
          I_m = eye(obj.m);

          if (isfield(obj.reuse, 'Cmat_chol'))
              Cmat_chol = obj.reuse.Cmat_chol;

              tic        
              IG = 2*sum(log(diag(Cmat_chol)));
              t = t + toc;
          else
              tic
              Cmat = I_m + obj.I_old * S_I * obj.I_old';
              t = t + toc;

              [IG, t1] = calcLnDet(Cmat);
              t = t + t1;
          end

          obj.unfocInfrmtn = -IG;
          obj.timing.infrmtn = t;
      end

      function calcFocusedInformation(obj)
          t = 0;
          obj.focInfrmtn = 0;

          if (~isempty(obj.focOldIndeces))
              S_I = obj.parent.getCov(obj.invVars);
              I_m = eye(obj.m);

              if (isfield(obj.reuse, 'Cmat_chol'))
                  Cmat_chol = obj.reuse.Cmat_chol;
                  
                  tic        
                  lndet1 = 2*sum(log(diag(Cmat_chol)));
                  t = t + toc;
              else
                  tic
                  Cmat = I_m + obj.I_old * S_I * obj.I_old';
                  t = t + toc;
                  
                  [lndet1, t1] = calcLnDet(Cmat);
                  t = t + t1;
              end

              if (isempty(obj.invUVars))
                  lndet2 = 0;
              else
                  if (isfield(obj.reuse, 'Cmat_chol_cond'))
                      Smat_chol = obj.reuse.Cmat_chol_cond;

                      tic
                      lndet2 = 2*sum(log(diag(Smat_chol)));
                      t = t + toc;
                  else
                      % Get conditional covariance
                      cov_cond = obj.parent.getCondCov(obj.invUVars);

                      tic
                      Smat = I_m + obj.I_U_old * cov_cond * obj.I_U_old';
                      t = t + toc;

                      [lndet2, t1] = calcLnDet(Smat);
                      t = t + t1;
                  end
              end

              IG = lndet1 - lndet2;
              obj.focInfrmtn = -IG;
          end;
          
          obj.timing.infrmtn = t;
      end

      function c = getColor(obj)
          c = 'magenta';
      end

    end
    
end

