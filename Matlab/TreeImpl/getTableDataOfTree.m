function [data] = getTableDataOfTree(node, data)
% Create data of tree for UI table

nodeData = cell(1, size(data, 2));
if (isa(node, 'BasicAction'))
    nodeData{1} = node.getName();
    nodeData{2} = class(node);
    nodeData{3} = node.isLeaf();
    nodeData{4} = node.parent.getName();
    nodeData{5} = int2str(node.n);
    nodeData{6} = int2str(node.k);
    nodeData{7} = int2str(node.m);
    nodeData{8} = int2str(node.I_old_dim);
    nodeData{9} = node.options.calcUnfoc;
    nodeData{10} = node.options.calcFoc;
    
    if (~isempty(node.options.focIndeces))
        nodeData{11} = mat2str(node.options.focIndeces);
        nodeData{12} = int2str(length(node.options.focIndeces));
    end
    
    nodeData{13} = num2str(node.unfocInfrmtn);
    if (node.isLeaf() && ~isnan(node.sumUnfocInfrmtn))
        nodeData{14} = num2str(node.sumUnfocInfrmtn);
    end

    nodeData{15} = num2str(node.focInfrmtn);
    if (node.isLeaf() && ~isnan(node.sumFocInfrmtn))
        nodeData{16} = num2str(node.sumFocInfrmtn);
    end

    if isfield(node.timing, 'cov')
        nodeData{17} = node.timing.cov;
    end

    if isfield(node.timing, 'cond_cov')
        nodeData{18} = node.timing.cond_cov;
    end

    if isfield(node.timing, 'infrmtn')
        nodeData{19} = node.timing.infrmtn;
    end

    allTime = node.getCompTime();
    if (allTime > 0)
        nodeData{20} = num2str(allTime, '%.10f');
    end
else
    nodeData{1} = node.getName();
    
    if isfield(node.timing, 'cov')
        nodeData{17} = node.timing.cov;
    end

    if isfield(node.timing, 'cond_cov')
        nodeData{18} = node.timing.cond_cov;
    end

    allTime = node.getCompTime();
    if (allTime > 0)
        nodeData{20} = num2str(allTime, '%.10f');
    end
end

data = [data; nodeData];

for i = 1:node.getChldrnNumber()
    child = node.getChild(i);
    data = getTableDataOfTree(child, data);
end

end

