classdef RectAction < BasicAction
    
    properties
    end
    
    methods
      function obj = RectAction(P, A, options)
          options.null = true;
          obj = obj@BasicAction(P, A, options);
      end
      
      function calcPosteriorCov(obj)
          
          t = 0;
          S_prior = obj.parent.getCov([obj.invVars, obj.Vold]);
          S_I = S_prior(1:length(obj.invVars), 1:length(obj.invVars));
          S_V = S_prior(length(obj.invVars)+1:end, length(obj.invVars)+1:end);
          S_C = S_prior(length(obj.invVars)+1:end, 1:length(obj.invVars));
          I_m = eye(obj.m);
          I_k = eye(obj.k);

          tic;
          F_inv = obj.new' * obj.new;
          t = t + toc;

          % Keep it - it may be required to calculate the information
          obj.reuse.F_inv = F_inv;

          [F, t1] = calcSymmInverse(F_inv);
          t = t + t1;
          
          tic;
          K = I_m - obj.new * F * obj.new';
          K1 = K * obj.I_old;
          t = t + toc;

          % Keep it - it may be required to calculate conditional cov
          obj.reuse.F = F;
          obj.reuse.K1 = K1;

          tic;
          C1 = obj.I_old * S_I * obj.I_old';
          G1 = K1 * S_I * K1';
          Cmat = I_m + C1;
          Gmat = I_m + G1;
          t = t + toc;
          
          [R, t1] = calcQForm(Gmat, K1');
          t = t + t1;

          tic;
          H11 = R'*R;
          H2 = S_C * H11;
          Sold = S_V - H2 * S_C';
          t = t + toc;

          [Cmat_chol, R1, t1] = calcCholAndQForm(Cmat, obj.new');
          t = t + t1;

          % Keep it - it may be required to calculate the information
          obj.reuse.Cmat_chol = Cmat_chol;

          tic;
          Snew_inv = R1'*R1;
          P = Snew_inv \ I_k(:,obj.Vnew);
          Snew = P(obj.Vnew,:);
          t = t + toc;

          % Keep it - it may be required to calculate the information
          obj.reuse.Snew_inv = Snew_inv;

          tic;
          Scross = (H2 * S_I - S_C) * ((obj.I_old' * obj.new) * F(:,obj.Vnew));
          t = t + toc;          
          
          obj.calcCov = [Sold, Scross; Scross', Snew];
          obj.timing.cov = t;
      end

      function calcPosteriorCondCovThroughPropagation(obj)
          t = 0;
          
          if (~isempty([obj.invUVars, obj.cond_Vold]))
              S_prior = obj.parent.getCondCov([obj.invUVars, obj.cond_Vold]);
              S_I = S_prior(1:length(obj.invUVars), 1:length(obj.invUVars));
              S_V = S_prior(length(obj.invUVars)+1:end, length(obj.invUVars)+1:end);
              S_C = S_prior(length(obj.invUVars)+1:end, 1:length(obj.invUVars));
          else
              S_I = zeros(0, 0);
              S_V = zeros(0, 0);
              S_C = zeros(0, 0);
          end
          I_m = eye(obj.m);
          I_k = eye(obj.k);

          if (isempty(obj.invUVars))
              Sold = S_V;
              Scross = zeros(length(obj.cond_Vold), length(obj.cond_Vnew));
              if (isfield(obj.reuse, 'F'))
                  F = obj.reuse.F;
                  Snew = F(obj.cond_Vnew, obj.cond_Vnew);
              else
                  tic;
                  Snew_inv = obj.new' * obj.new;
                  P = Snew_inv \ I_k(:,obj.cond_Vnew);
                  Snew = P(obj.cond_Vnew,:);
                  t = t + toc;
              end
          else
              if (isfield(obj.reuse, 'K1') && isfield(obj.reuse, 'F'))
                  F = obj.reuse.F;
                  idxs = arrayfun(@(x)find(obj.invVars==x,1),obj.invUVars);
                  K1 = obj.reuse.K1(:,idxs);
              else
                  tic;
                  F_inv = obj.new' * obj.new;
                  t = t + toc;

                  [F, t1] = calcSymmInverse(F_inv);
                  t = t + t1;

                  tic;
                  K = I_m - obj.new * F * obj.new';
                  K1 = K * obj.I_U_old;
                  t = t + toc;
              end

              tic;
              C1 = obj.I_U_old * S_I * obj.I_U_old';
              G1 = K1 * S_I * K1';
              Cmat = I_m + C1;
              Gmat = I_m + G1;
              t = t + toc;

              [R, t1] = calcQForm(Gmat, K1');
              t = t + t1;

              tic;
              H11 = R'*R;
              H2 = S_C * H11;
              Sold = S_V - H2 * S_C';
              t = t + toc;

              [Cmat_chol, R1, t1] = calcCholAndQForm(Cmat, obj.new');
              t = t + t1;

              % Keep it - it may be required to calculate the information
              obj.reuse.Cmat_chol_cond = Cmat_chol;

              tic;
              Snew_inv = R1'*R1;
              P = Snew_inv \ I_k(:,obj.cond_Vnew);
              Snew = P(obj.cond_Vnew,:);
              t = t + toc;

              % Keep it - it may be required to calculate the information
              obj.reuse.Snew_inv_cond = Snew_inv;

              tic;
              Scross = (H2 * S_I - S_C) * ((obj.I_U_old' * obj.new) * F(:,obj.cond_Vnew));
              t = t + toc;          
          end
          
          obj.calcCondCov = [Sold, Scross; Scross', Snew];
          obj.timing.cond_cov = t;
      end
          
      function calcUnfocusedInformation(obj)
          t = 0;
          obj.unfocInfrmtn = 0;
          
          S_I = obj.parent.getCov(obj.invVars);
          I_m = eye(obj.m);

          if (isfield(obj.reuse, 'Cmat_chol') && isfield(obj.reuse, 'Snew_inv'))
              Cmat_chol = obj.reuse.Cmat_chol;
              Snew_inv = obj.reuse.Snew_inv;              
          else
              tic;
              Cmat = I_m + obj.I_old * S_I * obj.I_old';
              t = t + toc;          
              
              [Cmat_chol, F, t1] = calcCholAndQForm(Cmat, obj.new');
              t = t + t1;

              tic        
              Snew_inv = F'*F;
              t = t + toc;
          end
          
          tic;
          lndet1 = 2*sum(log(diag(Cmat_chol)));
          t = t + toc;          

          [lndet2, t1] = calcLnDet(Snew_inv);
          t = t + t1;
          
          IG = lndet1 + lndet2;
          obj.unfocInfrmtn = -IG;
          obj.timing.infrmtn = t;
      end

      function calcFocusedInformation(obj)
          t = 0;
          obj.focInfrmtn = 0;
          
          if (~isempty(obj.focNewIndeces))
              S_I = obj.parent.getCov(obj.invVars);
              I_m = eye(obj.m);

              if (isfield(obj.reuse, 'Snew_inv'))
                  Snew_inv = obj.reuse.Snew_inv;
              else
                  tic;
                  Cmat = I_m + obj.I_old * S_I * obj.I_old';
                  t = t + toc;          

                  [F, t1] = calcQForm(Cmat, obj.new');
                  t = t + t1;
                  
                  tic        
                  Snew_inv = F'*F;
                  t = t + toc;
              end
              
              newUIndeces = obj.newUVars - obj.n;
              newFIndeces = setdiff([1:obj.k], newUIndeces);
              UF_order = [newUIndeces, newFIndeces];
              Snew_inv_UF_order = Snew_inv(UF_order, UF_order);
              
              tic                      
              R_UF_order = chol(Snew_inv_UF_order);
              eigvalues = diag(R_UF_order);
              entr = -2*sum(log(eigvalues((length(newUIndeces)+1):end)));
              t = t + toc;          
              
%               tic        
%               p1 = 2*sum(eigvalues(1:length(newUIndeces)));
%               p2 = 2*sum(eigvalues((length(newUIndeces)+1):end));
%               lndet1 = p1;
%               lndet2 = p1+p2;
%               entr = lndet1 - lndet2;
%               t = t + toc;          

              obj.focNewInfrmtn = entr;
              obj.focInfrmtn = obj.focInfrmtn + entr;
          end
          
          if (~isempty(obj.focOldIndeces))
              
              S_I = obj.parent.getCov(obj.invVars);
              I_m = eye(obj.m);

              if (isfield(obj.reuse, 'Cmat_chol') && isfield(obj.reuse, 'Snew_inv'))
                  Cmat_chol = obj.reuse.Cmat_chol;
                  Snew_inv = obj.reuse.Snew_inv;              
              else
                  tic;
                  Cmat = I_m + obj.I_old * S_I * obj.I_old';
                  t = t + toc;          

                  [Cmat_chol, F, t1] = calcCholAndQForm(Cmat, obj.new');
                  t = t + t1;

                  tic        
                  Snew_inv = F'*F;
                  t = t + toc;
              end

              tic;
              lndet1 = 2*sum(log(diag(Cmat_chol)));
              t = t + toc;          

              [lndet2, t1] = calcLnDet(Snew_inv);
              t = t + t1;

              if (isempty(obj.invUVars))
                  lndet3 = 0;
          
                  if (isfield(obj.reuse, 'F_inv'))
                      F_inv = obj.reuse.F_inv;
                  else
                      tic        
                      F_inv = obj.new'*obj.new;
                      t = t + toc;
                  end

                  [lndet4, t1] = calcLnDet(F_inv);
                  t = t + t1;
              else
                  % Get conditional covariance
                  cov_cond = obj.parent.getCondCov(obj.invUVars);
                  
                  if (isfield(obj.reuse, 'Cmat_chol_cond') && isfield(obj.reuse, 'Snew_inv_cond'))
                      Smat_chol = obj.reuse.Cmat_chol_cond;
                      SQ = obj.reuse.Snew_inv_cond;
                  else
                      % S matrix
                      tic
                      Smat = I_m + obj.I_U_old * cov_cond * obj.I_U_old';
                      t = t + toc;

                      [Smat_chol, R, t1] = calcCholAndQForm(Smat, obj.new');
                      t = t + t1;

                      tic        
                      SQ = R'*R;
                      t = t + toc;
                  end

                  tic;
                  lndet3 = 2*sum(log(diag(Smat_chol)));
                  t = t + toc;          

                  [lndet4, t1] = calcLnDet(SQ);
                  t = t + t1;              
              end
              
              IG = lndet1 + lndet2 - lndet3 - lndet4;
              obj.focOldInfrmtn = -IG;
              obj.focInfrmtn = obj.focInfrmtn - IG;
          end
          
          obj.timing.infrmtn = t;
      end

      function c = getColor(obj)
          c = 'blue';
      end

    end
    
end