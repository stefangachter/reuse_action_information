function [] = turnOnCalcFocOfTree(root, focIndeces, condCovMode)
% Turn on calcFoc of all nodes

root.options.calcFoc = true;
root.options.focIndeces = focIndeces;
root.options.condCovMode = condCovMode;

for i = 1:root.getChldrnNumber()
    child = root.getChild(i);
    
    turnOnCalcFocOfTree(child, focIndeces, condCovMode);
end

end

