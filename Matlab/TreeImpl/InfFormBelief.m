
classdef InfFormBelief < BasicBelief
    
    properties
        L
        foc_dim
        unfocIndeces
        unfoc_dim
        L_cond
    end
    
    methods
      function obj = InfFormBelief(L, options)
          options.null = true;
          obj = obj@BasicBelief(options);
          
          obj.L = L;
          obj.N = size(L, 1);
      end

      function [priorIndeces, priorCondIndeces] = checkReqPriorCovIndeces(obj)
          [priorIndeces, priorCondIndeces] = obj.checkReqPriorCovIndeces@BasicBelief();
          
          if (~isempty(priorCondIndeces))
              if (~isfield(obj.options, 'focIndeces') || isempty(obj.options.focIndeces))
                  warning('No variables are focused in the information-form belief!!!');
                  return;
              end
              
              obj.foc_dim = length(obj.options.focIndeces);
              obj.unfocIndeces = setdiff([1:obj.N], obj.options.focIndeces);
              obj.unfoc_dim = length(obj.unfocIndeces);
              obj.L_cond = obj.L(obj.unfocIndeces, obj.unfocIndeces);

              if ~isfield(obj.options, 'condCovMode')
                  if (obj.foc_dim > obj.unfoc_dim)
                      obj.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
                  else
                      obj.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
                  end
              end

              if (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_MARG))
                  priorIndeces = union(priorIndeces, priorCondIndeces)';
                  priorIndeces = union(priorIndeces, obj.options.focIndeces)';
              elseif (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_INFP))
                  % Do Nothing
              end
          end
      end

      function [] = computeCov(obj)
          t = 0;
          I_N = eye(obj.N);

          tic;
          P = obj.L \ I_N(:,obj.calcIndeces);
          cov = P(obj.calcIndeces,:);
          t = t + toc;

          obj.calcCov = cov;          
          obj.timing.cov = t;
      end

      function [] = computeCondCov(obj)
          t = 0;

          % Calculate conditional covariance
          if (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_MARG))
              UF_cov = obj.getCov([obj.calcCondIndeces, obj.options.focIndeces]);
              inn = 1:length(obj.calcCondIndeces);
              foc = [1:length(obj.options.focIndeces)] + max(inn);
              F_marg_cov = UF_cov(foc,foc);
              U_marg_cov = UF_cov(inn,inn);
              UF_marg_cov = UF_cov(inn,foc);

              [R, t1] = calcQForm(F_marg_cov, UF_marg_cov);
              t = t + t1;

              tic
              cov_cond = U_marg_cov - R'*R;
              t = t + toc;

          elseif (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_INFP))
              I_N = eye(obj.unfoc_dim);
              idxs = arrayfun(@(x)find(obj.unfocIndeces==x,1),obj.calcCondIndeces);
              
              tic;
              P = obj.L_cond \ I_N(:,idxs);
              cov_cond = P(idxs,:);
              t = t + toc;
          end

          obj.calcCondCov = cov_cond;
          obj.timing.cond_cov = t;
      end

    end
    
end

