function [t] = calcTimeOfTree(root)
% Get time of all calculations of tree

t = root.getCompTime();

for i = 1:root.getChldrnNumber()
    child = root.getChild(i);
    t = t + calcTimeOfTree(child);
end

end

