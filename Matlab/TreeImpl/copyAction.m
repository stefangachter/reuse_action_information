function [copy] = copyAction(a, P)

% options.calcUnfoc = a.options.calcUnfoc;
% options.calcFoc = a.options.calcFoc;
% options.focIndeces = a.options.focIndeces;

copy = NaN;
if isa(a, 'RectAction')
    copy = RectAction(P, a.A, a.options);
elseif isa(a, 'SquarAction')
    copy = SquarAction(P, a.A, a.options);
elseif isa(a, 'NAugAction')
    copy = NAugAction(P, a.A, a.options);
elseif isa(a, 'EmptyAction')
    copy = EmptyAction(P, a.options);
end

end

