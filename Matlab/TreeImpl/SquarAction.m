classdef SquarAction < BasicAction
    
    properties
    end
    
    methods
      function obj = SquarAction(P, A, options)
          options.null = true;
          obj = obj@BasicAction(P, A, options);
          
          if (obj.m ~= obj.k)
              error('The provided Jacobian is not appropriate for Squared Action');
          end
      end
            
      function calcPosteriorCov(obj)
          t = 0;
          S_prior = obj.parent.getCov([obj.invVars, obj.Vold]);
          S_I = S_prior(1:length(obj.invVars), 1:length(obj.invVars));
          S_V = S_prior(length(obj.invVars)+1:end, length(obj.invVars)+1:end);
          S_C = S_prior(length(obj.invVars)+1:end, 1:length(obj.invVars));
          I_m = eye(obj.m);
          I_k = eye(obj.k);

          tic
          Cmat = I_m + obj.I_old * S_I * obj.I_old';
          t = t + toc;

          % Keep it - it may be required to calculate the information
          obj.reuse.Cmat = Cmat;

          tic
          A_iv = obj.new' \ I_k(:,obj.Vnew);
          A_iv = A_iv';
          t = t + toc;

          tic
          Snew = A_iv * Cmat * A_iv';
          H = obj.I_old' * A_iv';
          Scross = - S_C * H;
          t = t + toc;

          % Keep it - it may be required to calculate conditional cov
          obj.reuse.A_iv = A_iv;
          obj.reuse.H = H;

          obj.calcCov = [S_V, Scross; Scross', Snew];
          obj.timing.cov = t;
      end

      function calcPosteriorCondCovThroughPropagation(obj)
          t = 0;
          S_prior = obj.parent.getCondCov([obj.invUVars, obj.cond_Vold]);
          S_I = S_prior(1:length(obj.invUVars), 1:length(obj.invUVars));
          S_V = S_prior(length(obj.invUVars)+1:end, length(obj.invUVars)+1:end);
          S_C = S_prior(length(obj.invUVars)+1:end, 1:length(obj.invUVars));
          I_m = eye(obj.m);
          I_k = eye(obj.k);

          tic
          Cmat = I_m + obj.I_U_old * S_I * obj.I_U_old';
          t = t + toc;

          % Keep it - it may be required to calculate the information
          %obj.reuse.Cmat = Cmat;

          if (isequal(obj.Vnew, obj.cond_Vnew) && isfield(obj.reuse, 'A_iv') && isfield(obj.reuse, 'H'))
              A_iv = obj.reuse.A_iv;
              idxs = arrayfun(@(x)find(obj.invVars==x,1),obj.invUVars);
              H = obj.reuse.H(idxs,:);
          elseif (all(ismember(obj.cond_Vnew,obj.Vnew)) && isfield(obj.reuse, 'A_iv') && isfield(obj.reuse, 'H'))
              A_iv = obj.reuse.A_iv;
              new_idxs = arrayfun(@(x)find(obj.Vnew==x,1),obj.cond_Vnew);
              A_iv = A_iv(new_idxs,:);

              old_idxs = arrayfun(@(x)find(obj.invVars==x,1),obj.invUVars);
              H = obj.reuse.H;
              H = H(old_idxs, new_idxs);
          else
              tic
              A_iv = obj.new' \ I_k(:,obj.cond_Vnew);
              A_iv = A_iv';
              H = obj.I_U_old' * A_iv';
              t = t + toc;              
          end
          
          tic
          Snew = A_iv * Cmat * A_iv';
          Scross = - S_C * H;
          t = t + toc;

          obj.calcCondCov = [S_V, Scross; Scross', Snew];
          obj.timing.cond_cov = t;
      end

      function calcUnfocusedInformation(obj)
          t = 0;

          tic
          IG = 2*log(abs(det(obj.new)));
          t = t + toc;

          obj.unfocInfrmtn = -IG;
          obj.timing.infrmtn = t;
      end

      function calcFocusedInformation(obj)
          t = 0;
          obj.focInfrmtn = 0;

          if (~isempty(obj.focNewIndeces))
              S_I = obj.parent.getCov(obj.invVars);
              I_m = eye(obj.m);

              if (isfield(obj.reuse, 'Cmat'))
                  Cmat = obj.reuse.Cmat;
              else
                  tic
                  Cmat = I_m + obj.I_old * S_I * obj.I_old';
                  t = t + toc;
              end
              
              [F, t1] = calcQForm(Cmat, obj.new');
              t = t + t1;
              tic
              Snew_inv = F'*F;
              t = t + toc;
                  
              newUIndeces = obj.newUVars - obj.n;
              newFIndeces = setdiff([1:obj.k], newUIndeces);
              UF_order = [newUIndeces, newFIndeces];
              Snew_inv_UF_order = Snew_inv(UF_order, UF_order);
              
              tic                      
              R_UF_order = chol(Snew_inv_UF_order);
              eigvalues = diag(R_UF_order);
              entr = -2*sum(log(eigvalues((length(newUIndeces)+1):end)));
              t = t + toc;          

              obj.focNewInfrmtn = entr;
              obj.focInfrmtn = entr;
          end
          
          if (~isempty(obj.focOldIndeces))
              IG = 0;
              obj.focOldInfrmtn = -IG;
              obj.focInfrmtn = obj.focInfrmtn - IG;
          end
          
          obj.timing.infrmtn = t;
      end

      function flag = needPriorCondCovForInvUVars(obj)
          flag = false;
      end

      function c = getColor(obj)
          c = 'cyan';
      end

    end
    
end

