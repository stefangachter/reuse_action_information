%clear all;
close all;

rng(100);

n = 2000;

% joint state
A_all = 0.1*rand(10*n,n);

% determine reordering so that eigenvalues appear sorted
% [~, ~,P]=qr(A_all);
% A_all = A_all*P;
info_mat = A_all'*A_all;
cov_mat = inv(info_mat);

% Init action counter
BasicAction.getNextActionCounter(0);

%cb = CovFormBelief(cov_mat);
%condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
options.condCovMode = condCovMode;
cb = InfFormBelief(info_mat, options);


a1 = generateRectAction(cb, 200, 100);
[a11, a12] = generateTwoTypeActions(a1, 500, 200);
%a11 = generateRectAction(a1, 200, 100);
a111 = generateSquarAction(a11, 100);
a121 = copyAction(a111, a12);

a13 = generateSquarAction(a1, 100);

a2 = generateSquarAction(a1, 100);
a2_empty = generateEmptyAction(a2);
a3 = generateNAugAction(a2, 100);
a31 = generateRectAction(a3, 200, 100);
a32 = generateRectAction(a3, 200, 100);

a5 = generateRectAction(a2, 200, 100);
a6 = generateRectAction(a5, 200, 100);
a7 = generateRectAction(a6, 200, 100);

a71 = generateNAugAction(a7, 100);
a72 = generateNAugAction(a7, 100);
a73 = generateNAugAction(a7, 100);
a74 = generateNAugAction(a7, 100);

turnOnCalcFocOfTree(cb, 10:60, condCovMode);


flatCopy = flatTreeCopy(cb);

cb.propagate();
flatCopy.propagate();

agregateInf(cb);
agregateInf(flatCopy);


if (~isInformtnIdentical({cb, flatCopy}))
    warning('Two trees have different results!!!');
end

% printTreeResults(cb);
% printTreeResults(flatCopy);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
%subplot(2,1,1);
plotTree(cb, true);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plotTableOfActions(cb);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
%subplot(2,1,2);
plotTree(flatCopy, true);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plotTableOfActions(flatCopy);
