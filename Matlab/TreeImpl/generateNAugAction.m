function [ a ] = generateNAugAction(P, m)

J = zeros(m,P.N);

% Selecting involved variables to be in number equal to 10% of dimension
%invNum = ceil(P.N/10);
invNum = 50;
invIndcs = randperm(P.N, invNum);
J(:,invIndcs) = rand(m, invNum);
options.calcUnfoc = false;
options.calcFoc = false;
options.focIndeces = [];
a = NAugAction(P, J, options);

end

