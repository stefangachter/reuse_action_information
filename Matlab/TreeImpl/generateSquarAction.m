function [ a ] = generateSquarAction(P, k)

J = zeros(k,P.N+k);
J(:,P.N+1:P.N+k) = rand(k, k);

% Selecting involved variables to be last old one
invIndcs = (P.N-3):P.N;
J(:,invIndcs) = rand(k, 4);
options.calcUnfoc = false;
options.calcFoc = false;
options.focIndeces = [];
a = SquarAction(P, J, options);

end

