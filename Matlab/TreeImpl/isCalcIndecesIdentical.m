function [is_ident] = isCalcIndecesIdentical(nodes)
% Check if nodes have calculated same covariance indeces

is_ident = true;
nodesNum = length(nodes);

if isfield(nodes{1}.options, 'focIndeces')
    cov_ideces = setsUnionMultiple({nodes{1}.calcIndeces, nodes{1}.calcCondIndeces, nodes{1}.options.focIndeces});
    for i = 2:nodesNum
        curr_cov_ideces = setsUnionMultiple({nodes{i}.calcIndeces, nodes{i}.calcCondIndeces, nodes{i}.options.focIndeces});

        if (~isequal(cov_ideces, curr_cov_ideces))
            is_ident = false;
            return;
        end
    end
else
    cov_ideces = setsUnionMultiple({nodes{1}.calcIndeces, nodes{1}.calcCondIndeces});
    for i = 2:nodesNum
        curr_cov_ideces = setsUnionMultiple({nodes{i}.calcIndeces, nodes{i}.calcCondIndeces});

        if (~isequal(cov_ideces, curr_cov_ideces))
            is_ident = false;
            return;
        end
    end
end

end

