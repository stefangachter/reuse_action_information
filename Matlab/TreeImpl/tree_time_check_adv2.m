clear all;
close all;

rng(100);

n = 200;

% joint state
A_all = 0.1*rand(10*n,n);

% determine reordering so that eigenvalues appear sorted
[~, ~,P]=qr(A_all);
A_all = A_all*P;
info_mat = A_all'*A_all;
cov_mat = inv(info_mat);


A1.n_before = n;
A1.k = 60;
A1.N = A1.n_before+A1.k;
A1.m = 60;
A1.J = zeros(A1.m, A1.N);
A1.J(:,A1.n_before+1:A1.N) = rand(A1.m, A1.k);
A1.J(:,A1.n_before) = rand(A1.m, 1);
%A1.J(:,1:10) = rand(A1.m, 10);
%A.J = 100*A.J;
A1.old = A1.J(:, 1:A1.n_before);
A1.new = A1.J(:, A1.n_before+1:end);
A1.zeroVars = find(sum(abs(A1.old)) == 0);
A1.invVars = setdiff(1:A1.n_before,A1.zeroVars);
A1.I_old = A1.old(:,A1.invVars);
A1.I_old_dim = length(A1.invVars);

A2.n_before = A1.N;
A2.k = 0;
A2.N = A2.n_before+A2.k;
A2.m = 80;
A2.J = zeros(A2.m, A2.n_before+A2.k);
A2.J(:,1:20) = rand(A2.m, 20);
A2.J(:,end-19:end) = rand(A2.m, 20);
%A.J = 100*A.J;
A2.old = A2.J(:, 1:A2.n_before);
A2.new = A2.J(:, A2.n_before+1:end);
A2.zeroVars = find(sum(abs(A2.old)) == 0);
A2.invVars = setdiff(1:A2.n_before,A2.zeroVars);
A2.I_old = A2.old(:,A2.invVars);
A2.I_old_dim = length(A2.invVars);

A.n_before = A1.n_before;
A.k = A1.k + A2.k;
A.N = A.n_before+A.k;
A.m = A1.m + A2.m;
A.J = zeros(A.m, A.N);
A.J(1:A1.m,1:A1.N) = A1.J;
A.J(A1.m+1:end,1:A2.N) = A2.J;
A.old = A.J(:, 1:A.n_before);
A.new = A.J(:, A.n_before+1:end);
A.zeroVars = find(sum(abs(A.old)) == 0);
A.invVars = setdiff(1:A.n_before,A.zeroVars);
A.I_old = A.old(:,A.invVars);
A.I_old_dim = length(A.invVars);

B.n_before = A.N;
B.k = 60;
B.N = B.n_before+B.k;
B.m = 120;
B.J = zeros(B.m, B.N);
B.J(:,B.n_before+1:B.N) = rand(B.m, B.k);
B.J(:,B.n_before) = rand(B.m, 1);
B.J(:,1:B.k) = rand(B.m, B.k);
%B.J = 100*B.J;
B.old = B.J(:, 1:B.n_before);
B.new = B.J(:, B.n_before+1:end);
B.zeroVars = find(sum(abs(B.old)) == 0);
B.invVars = setdiff(1:B.n_before,B.zeroVars);
B.I_old = B.old(:,B.invVars);
B.I_old_dim = length(B.invVars);


C.n_before = A.n_before;
C.k = A.k + B.k;
C.N = B.N;
C.m = A.m + B.m;
C.J = zeros(C.m, C.N);
C.J(1:A.m,1:A.N) = A.J;
C.J(A.m+1:end,1:B.N) = B.J;
C.old = C.J(:, 1:C.n_before);
C.new = C.J(:, C.n_before+1:end);
C.zeroVars = find(sum(abs(C.old)) == 0);
C.invVars = setdiff(1:C.n_before,C.zeroVars);
C.I_old = C.old(:,C.invVars);
C.I_old_dim = length(C.invVars);

% Calculating usual way
info_mat_aug = zeros(C.N,C.N);
info_mat_aug(1:C.n_before,1:C.n_before) = info_mat;
post_info_mat = info_mat_aug + C.J'*C.J;
post_cov_mat = inv(post_info_mat);
last_pose_cov = post_cov_mat(end-1:end,end-1:end);
last_pose_lndet = log(det(last_pose_cov));

% Clculate V
V = B.invVars;
Vold = B.invVars(find(B.invVars <= A.n_before));
Vnew = setdiff(B.invVars, Vold) - A.n_before;

% Calculating cov specific entries at the middle
S_I = cov_mat(A.invVars,A.invVars);
S_V = cov_mat(Vold,Vold);
S_C = cov_mat(Vold,A.invVars);
I_m = eye(A.m);
I_k = eye(A.k);

F = inv(A.new' * A.new);
K = I_m - A.new * F * A.new';
K1 = K * A.I_old;

C1 = A.I_old * S_I * A.I_old';
G1 = K1 * S_I * K1';
C = I_m + C1;
C_inv = inv(C);
G = I_m + G1;
G_inv = inv(G);

H1 = K1' * G_inv * K1;
H2 = S_C * H1;
Sold = S_V - H2 * S_C';

Snew_inv = A.new' * C_inv * A.new;
P = Snew_inv \ I_k(:,Vnew);
Snew = P(Vnew,:);

Scross = (H2 * S_I - S_C) * ((A.I_old' * A.new) * F(:,Vnew));
cov_mat_inv_middle = [Sold, Scross; Scross', Snew];

% Calculate lndet
Cmat = eye(B.m) + B.I_old * cov_mat_inv_middle * B.I_old';
Cmat_inv = inv(Cmat);

B_U_new = B.new(:,1:end-2);

lndet1 = log(det(B_U_new' * Cmat_inv * B_U_new));
lndet2 = log(det(B.new' * Cmat_inv * B.new));

last_pose_lndet_through_AB = lndet1 - lndet2;

max(max(abs(last_pose_lndet - last_pose_lndet_through_AB)))

% Clculate V 2
V = B.invVars;
Vold = V(find(V <= A2.n_before));
Vnew = setdiff(V, Vold) - A.n_before;

V2 = [A2.invVars, Vold];
Vold2 = V2(find(V2 <= A1.n_before));
Vnew2 = setdiff(V2, Vold2) - A1.n_before;


% Get required covariances at the middle
Sold_2 = cov_mat(Vold2, Vold2);
cov_mat_inv_A1 = cov_mat(A1.invVars, A1.invVars);

Cmat = eye(A1.m) + A1.I_old * cov_mat_inv_A1 * A1.I_old';

e = zeros(A1.m,1);
e(Vnew2) = 1;

A1_iv = A1.new' \ e;
A1_iv = A1_iv';

Snew_2 = A1_iv * Cmat * A1_iv';

cov_mat_inv_A1 = cov_mat(A1.invVars, Vold2);

Scross_2 = -(A1_iv * A1.I_old * cov_mat_inv_A1)';

cov_mat_2 = [Sold, Scross; Scross', Snew];
