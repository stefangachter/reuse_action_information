function [] = turnOnCalcUnfocOfTree(root)
% Turn on calcUnfoc of all nodes

root.options.calcUnfoc = true;

for i = 1:root.getChldrnNumber()
    child = root.getChild(i);
    turnOnCalcUnfocOfTree(child);
end

end

