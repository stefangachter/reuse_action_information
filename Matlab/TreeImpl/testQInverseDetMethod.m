clear all;
close all;

rng(100);

addpath(genpath('invChol'));
t_vec1 = [];
t_vec2 = [];
t_vec3 = [];
t_vec4 = [];

%dims = [5:200, 201:10:500, 501:100:2000];
dims = [10:10];
for i = dims
    % joint state
    A_all = 0.1*rand(10*i,i);

    % determine reordering so that eigenvalues appear sorted
    [~, ~,P]=qr(A_all);
    A_all = A_all*P;
    M = A_all'*A_all;

    A = 0.1*rand(ceil(i/2),i);
    B = A(1:end-2,:);
    C = A(end-1:end,:);

    dim = size(M, 1);
    t1 = 0;
    t2 = 0;
    t3 = 0;
    t4 = 0;

    [M_inv, t1] = calcSymmInverse(M);
    tic    
    C1 = A * M_inv * A';
    t1 = t1 + toc;
    [lndet1, t11] = calcLnDet(C1);
    t1 = t1 + t11;
    tic    
    C2 = B * M_inv * B';
    t1 = t1 + toc;
    [lndet11, t11] = calcLnDet(C2);
    t1 = t1 + t11;
    C3 = C * M_inv * C';
    [lndet13, t11] = calcLnDet(C3);
    lndet_f1 = lndet11 - lndet1;
    t_vec1 = [t_vec1 t1];
    
    [F, t2] = calcQForm(M, A);
    tic
    R = qr(F,0);
    dd = log(abs(diag(R)));
    p1 = 2*sum(dd(1:end-2));
    p2 = 2*sum(dd(end-1:end));
    lndet2 = p1+p2;
    lndet22 = p1;
    t2 = t2 + toc;
    lndet_f2 = lndet22 - lndet2;
    lndet23 = p2;
    R = triu(R);
    H = R(:,1:end-2);
    G = R(:,end-1:end);

    t_vec2 = [t_vec2 t2];

    [F, t3] = calcQForm(M, A);
    tic        
    V = chol(F'*F);
    dd = log(diag(V));
    p1 = 2*sum(dd(1:end-2));
    p2 = 2*sum(dd(end-1:end));
    lndet3 = p1+p2;
    lndet33 = p1;
    t3 = t3 + toc;
    t_vec3 = [t_vec3 t3];

    [F, t4] = calcQForm(M, A);
    tic
    C1 = F'*F;
    t4 = t4 + toc;
    [V, t41] = calcChol(C1);
    t4 = t4 + t41;
    tic        
    %dec = chol(F'*F);
    dd = log(diag(V));
    lndet4 = 2*sum(dd);
    lndet44 = 2*sum(dd(1:end-2));
    t4 = t4 + toc;
    t_vec4 = [t_vec4 t4];
    
%     huptriang = dsp.UpperTriangularSolver;
% 
%     tic
%     RR = chol(M);
%     VV1 = huptriang(RR', A')
%     V4 = VV1'*VV1;
%     t4 = t4 + toc;
%    t_vec4 = [t_vec4 t4];
end

indeces = 1:(length(t_vec1));
plot(dims, t_vec1, dims, t_vec2, dims, t_vec3, dims, t_vec4);
legend('inverse', 'qfrm and qr', 'qfrm and lndet', '4');