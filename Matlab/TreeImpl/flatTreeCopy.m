function [ copy ] = flatTreeCopy( cb )

if isa(cb, 'CovFormBelief')
    copy = CovFormBelief(cb.S, cb.options);
elseif isa(cb, 'InfFormBelief')
    copy = InfFormBelief(cb.L, cb.options);
elseif isa(cb, 'GtsamBelief')
    copy = GtsamBelief(cb.graph, cb.values, cb.options);
end

J = zeros(0, cb.N);
flatChildren(copy, cb, J);

end

