function [cdata] = colorCheckbox(bcolor,flag)
% Color checkbox for uitable

bcolorPart = '';
if ~isempty(bcolor)
    colorPart = ['color=',bcolor];
    bcolorPart = ['bgcolor=',bcolor];
end

checkV = '';
if (flag)
    checkV = 'checked';
end
chkbox = ['<input type="checkbox" ', checkV, ' ',bcolorPart,' ',colorPart,'>'];
cdata = ['<html><div width=40 align="center" ',bcolorPart,' ',colorPart,'>', chkbox, '</div></html>'];
%cdata = ['<html><table border=0 width=400 ',bcolorPart,'><TR><TD> ',chkbox,' </TD></TR> </table></html>'];

end

