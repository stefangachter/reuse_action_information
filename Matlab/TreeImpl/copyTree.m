function [copy] = copyTree(root)
% Copy tree

copy = NaN;
if isa(root, 'CovFormBelief')
    copy = CovFormBelief(root.S, root.options);
elseif isa(root, 'InfFormBelief')
    copy = InfFormBelief(root.L, root.options);
elseif isa(root, 'GtsamBelief')
    copy = GtsamBelief(root.graph, root.values, root.options);
end

copyChildren(root, copy);

end

