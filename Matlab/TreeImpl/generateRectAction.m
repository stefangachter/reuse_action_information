function [ a ] = generateRectAction(P, m, k )

J = zeros(m,P.N+k);
J(:,P.N+1:P.N+k) = rand(m, k);

% Selecting involved variables to be in number equal to 10% of dimension
%invNum = ceil(P.N/10);
invNum = 50;
invIndcs = randperm(P.N, invNum);
J(:,invIndcs) = rand(m, invNum);
options.calcUnfoc = false;
options.calcFoc = false;
options.focIndeces = [];
a = RectAction(P, J, options);

end

