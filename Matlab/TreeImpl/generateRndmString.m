function [ str ] = generateRndmString( stLength )

%symbols = ['a':'z' 'A':'Z' '0':'9'];
symbols = ['a':'z' '0':'9'];
nums = randi(numel(symbols),[1 stLength]);
str = symbols (nums);

end

