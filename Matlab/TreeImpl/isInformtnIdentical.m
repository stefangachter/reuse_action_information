function [is_ident] = isInformtnIdentical(roots)
% Check if both trees have the same information at their leafs

is_ident = true;
rootsNum = length(roots);

infArrs = cell(1, rootsNum);
nanIs = cell(1, rootsNum);
not_nanValues = cell(1, rootsNum);

for i = 1:rootsNum
    infArr = zeros(2,0);
    infArr = getLeafInformation(roots{i}, infArr);
    infArrs{i} = infArr;
    
    nanI = find(isnan(infArr));
    nanIs{i} = nanI;
    
    not_nanI = find(~isnan(infArr));
    not_nanValues{i} = infArr(not_nanI);
end

if ~isequal(nanIs{:})
    is_ident = false;
    return;
end

ress = [not_nanValues{:}]';
D = pdist(ress, 'chebychev');
D = squareform(D);

% Normalize distance by information maximum value
D = D/max(max(abs(ress)));

if (max(max(D)) > 10^(-3))
    is_ident = false;
end


end

