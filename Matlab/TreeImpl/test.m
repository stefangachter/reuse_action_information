clear all;
close all;

rng(100);

n = 5;

% joint state
A_all = 0.1*rand(10*n,n);

% determine reordering so that eigenvalues appear sorted
[~, ~,P]=qr(A_all);
A_all = A_all*P;
info_mat = A_all'*A_all;
cov_mat = inv(info_mat);

% Init action counter
BasicAction.getNextActionCounter(0);


%cb = CovFormBelief(cov_mat);
%options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
cb = InfFormBelief(info_mat, options);

options.calcUnfoc = false;
options.calcFoc = false;
options.focIndeces = [];

m = 8;
k = 4;
J = zeros(m,n+k);
J(:,n+1:n+k) = rand(m, k);
J(:,3:4) = rand(m, 2);
a = RectAction(cb, J, options);

n = n + k;
m = 9;
k = 9;
J = zeros(m,n+k);
J(:,n+1:n+k) = rand(m, k);
J(:,2:6) = rand(m, 5);
a1 = SquarAction(a, J, options);

n = n + k;
m = 15;
k = 0;
J = zeros(m,n+k);
J(:,5:10) = rand(m, 6);
a2 = NAugAction(a1, J, options);


n = n + k;
m = 9;
k = 5;
J = zeros(m,n+k);
J(:,n-4:n+k) = rand(m, k+5);
J(:,4:6) = rand(m, 3);
b = RectAction(a2, J, options);
b.options.calcFoc = true;
b.options.focIndeces = (b.N-1):(b.N);



cb.propagate();
agregateInf(cb);


m = a.m + a1.m + a2.m + b.m;
N = b.N;
J = zeros(m,N);
J(1:a.m, 1:a.N) = a.A;
J(a.m+1:a.m+a1.m, 1:a1.N) = a1.A;
J(a.m+a1.m+1:a.m+a1.m+a2.m, 1:a2.N) = a2.A;
J(a.m+a1.m+a2.m+1:end, 1:b.N) = b.A;

% Calculating usual way
info_mat_aug = zeros(b.N,b.N);
info_mat_aug(1:cb.N,1:cb.N) = info_mat;
post_info_mat = info_mat_aug + J'*J;
post_cov_mat = inv(post_info_mat);
last_pose_cov = post_cov_mat(end-1:end,end-1:end);
last_pose_lndet = log(det(last_pose_cov));


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
%subplot(2,1,1);
plotTree(cb, true);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plotTableOfActions(cb);


b.sumFocInfrmtn

last_pose_lndet