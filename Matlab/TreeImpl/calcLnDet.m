function [ ld, t ] = calcLnDet( M )

dim = size(M, 1);
t = 0;

if (dim == 1)
    ld = log(M);
elseif (dim <= 35)
    tic
    ld = log(det(M));
    t = toc;
else
    tic        
    dec = chol(M);
    
    ld = 2*sum(log(diag(dec)));
    t = toc;
end


end

