function [V, t] = calcSymmInverse(M)

addpath(genpath('invChol'));

dim = size(M, 1);
t = 0;

if (dim >= 100)
    tic
    V = invChol_mex(M);
    t = t + toc;
else
    I = eye(dim);
    tic
    R = chol(M);
    V = R\(R'\I);
    t = t + toc;
end

end

