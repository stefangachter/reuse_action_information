
classdef TREE_CONSTANTS
    
   properties (Constant)
      COND_COV_MODE_MARG = 'ThroughMarginal';
      COND_COV_MODE_INFP = 'ThroughInfPartition';
   end
    
end

