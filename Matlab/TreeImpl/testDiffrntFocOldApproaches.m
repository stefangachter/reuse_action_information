%clear all;
close all;

rng(100);

n = 3000;

% joint state
A_all = 0.1*rand(10*n,n);

% determine reordering so that eigenvalues appear sorted
% [~, ~,P]=qr(A_all);
% A_all = A_all*P;
info_mat = A_all'*A_all;
cov_mat = inv(info_mat);

% Init action counter
BasicAction.getNextActionCounter(0);

%cb = CovFormBelief(cov_mat);
%condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
options.condCovMode = condCovMode;
cb = InfFormBelief(info_mat, options);


a1 = generateRectAction(cb, 200, 100);
[a11, a12] = generateTwoTypeActions(a1, 500, 200);
%a11 = generateRectAction(a1, 200, 100);
a111 = generateSquarAction(a11, 100);
a121 = copyAction(a111, a12);

a13 = generateSquarAction(a1, 100);

a2 = generateSquarAction(a1, 100);
a3 = generateNAugAction(a2, 100);
a31 = generateRectAction(a3, 200, 100);
a32 = generateRectAction(a3, 200, 100);

a5 = generateRectAction(a2, 200, 100);
a6 = generateRectAction(a5, 200, 100);
a7 = generateRectAction(a6, 200, 100);

a71 = generateNAugAction(a7, 100);
a72 = generateNAugAction(a7, 100);
a73 = generateNAugAction(a7, 100);
a74 = generateNAugAction(a7, 100);

turnOnCalcFocOfTree(cb, 10:60, condCovMode);

fi = n-10:n;

times1 = [];
times2 = [];
times3 = [];
times4 = [];
times5 = [];
times6 = [];

for j = fi

    focIndeces = 2:j;

    cb1 = copyTree(cb);
    turnOnCalcFocOfTree(cb1, focIndeces, TREE_CONSTANTS.COND_COV_MODE_MARG);
    cb1.propagate();

    cb2 = copyTree(cb);
    turnOnCalcFocOfTree(cb2, focIndeces, TREE_CONSTANTS.COND_COV_MODE_INFP);
    cb2.propagate();

    cb3 = flatTreeCopy(cb);
    turnOnCalcFocOfTree(cb3, focIndeces, TREE_CONSTANTS.COND_COV_MODE_MARG);
    cb3.propagate();

    cb4 = flatTreeCopy(cb);
    turnOnCalcFocOfTree(cb4, focIndeces, TREE_CONSTANTS.COND_COV_MODE_INFP);
    cb4.propagate();

    cb5 = copyTree(cb);
    turnOnCalcFocOfTree(cb5, focIndeces, TREE_CONSTANTS.COND_COV_MODE_MARG);
    cb5.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
    cb5.propagate();

    cb6 = copyTree(cb);
    turnOnCalcFocOfTree(cb6, focIndeces, TREE_CONSTANTS.COND_COV_MODE_INFP);
    cb6.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
    cb6.propagate();

    
    if (~isInformtnIdentical({cb1, cb2, cb3, cb4, cb5, cb6}))
        warning('Trees have different results!!!');
    end

    times1 = [times1, cb1.timing.total];
    times2 = [times2, cb2.timing.total];
    times3 = [times3, cb3.timing.total];
    times4 = [times4, cb4.timing.total];
    times5 = [times5, cb5.timing.total];
    times6 = [times6, cb6.timing.total];

end

plot(fi, times1, fi, times2, fi, times3, fi, times4, fi, times5, fi, times6);
legend('1', '2', '3', '4', '5', '6');


% if (~isInformtnIdentical(cb, cb1))
%     warning('Two trees have different results!!!');
% end
% 
% % printTreeResults(cb);
% % printTreeResults(flatCopy);
% 
% 
% Hfig = figure();
% set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
% %subplot(2,1,1);
% plotTree(cb, true);
% 
% 
% Hfig = figure();
% set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
% plotTableOfActions(cb);
% 
% 
% Hfig = figure();
% set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
% %subplot(2,1,2);
% plotTree(cb1, true);
% 
% 
% Hfig = figure();
% set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
% plotTableOfActions(cb1);
