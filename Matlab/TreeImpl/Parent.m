
classdef Parent < handle
    
    properties
        children
    end
    
    methods
      function obj = Parent()
         obj.children = {};
      end
      
      function addChild(obj, child)
          obj.children{length(obj.children) + 1} = child;
      end

      function num = getChldrnNumber(obj)
          num = length(obj.children);
      end

      function child = getChild(obj, i)
          child = obj.children{i};
      end

      function res = isLeaf(obj)
          res = (length(obj.children) == 0);
      end

    end
    
end