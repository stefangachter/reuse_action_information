function [arr, nodes] = buildTreeArray(arr, nodes, p, node)

curr_id = length(arr) + 1;

arr(curr_id) = p;
nodes{curr_id} = node;

for i = 1:node.getChldrnNumber()
    child = node.getChild(i);
    [arr, nodes] = buildTreeArray(arr, nodes, curr_id, child);
end


end

