function [] = copyChildren(root, newParent)
% Copy children

for i = 1:root.getChldrnNumber()
    child = root.getChild(i);
    copyChild = copyAction(child, newParent);
    copyChildren(child, copyChild);
end

end

