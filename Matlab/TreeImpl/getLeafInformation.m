function [infArr] = getLeafInformation(node, infArr)
% Retrieve information of the leafs from tree

if (node.isLeaf())
    infArr = [infArr, [node.sumUnfocInfrmtn; node.sumFocInfrmtn]];
else
    for i = 1:node.getChldrnNumber()
        child = node.getChild(i);
        infArr = getLeafInformation(child, infArr);
    end
end

end

