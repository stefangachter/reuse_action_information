function [ctext] = colorText(color,bcolor,text)
% Color text for uitable

colorPart = '';
if ~isempty(color)
    colorPart = ['color=',color];
end

bcolorPart = '';
if ~isempty(bcolor)
    bcolorPart = ['bgcolor=',bcolor];
end

%ctext = ['<html><table border=0 width=400 ',colorPart,' ',bcolorPart,'><TR><TD>',text,'</TD></TR> </table></html>'];
ctext = ['<html><div width="3000" ',colorPart,' ',bcolorPart,'> &nbsp;', text, '</div></html>'];

% CLR_RED = '#FF0000';
% CLR_GREEN = '#00FF00';
% CLR_BLUE = '#0000FF';
% CLR_BLACK = '#000000';
% CLR_WHITE = '#FFFFFF';

end

