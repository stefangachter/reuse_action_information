
classdef BasicBelief < BSPVertex
    
    properties
        N
        options
    end
    
    methods
      function obj = BasicBelief(options)
          obj = obj@BSPVertex();
         
         if nargin > 0
            obj.options = options;
         else
             obj.options.null = true;
         end
      end
      
      function propagate(obj)
          % What covariance entries are required (marginal and conditional)
          [priorIndeces, priorCondIndeces] = checkReqPriorCovIndeces(obj);
          
          % Calculate required marginal covariance
          obj.calcIndeces = priorIndeces;
          obj.computeCov();

          % Calculate required conditional covariance
          obj.calcCondIndeces = priorCondIndeces;
          if (~isempty(obj.calcCondIndeces))
              if (~isfield(obj.options, 'focIndeces') || isempty(obj.options.focIndeces))
                  warning('No variables are focused in the current belief!!!');
              else
                  obj.computeCondCov();
              end
          end
          
          % Propagate children's calculation
          for i = 1:obj.getChldrnNumber()
              child = obj.getChild(i);
              child.propagate();
          end
          
          agregateInf(obj);
          obj.timing.total = calcTimeOfTree(obj);
      end

      function [priorIndeces, priorCondIndeces] = checkReqPriorCovIndeces(obj)
          % Analyze what covariances (marginal and conditional)
          % are required by children
          priorIndeces = [];
          priorCondIndeces = [];
          for i = 1:obj.getChldrnNumber()
              child = obj.getChild(i);
              [currIndeces, currCondIndeces] = child.checkReqPriorCovIndeces();
              priorIndeces = setsUnion(priorIndeces, currIndeces);
              priorCondIndeces = setsUnion(priorCondIndeces, currCondIndeces);
          end
      end
      
      function name = getName(obj)
          name = 'Root - Belief at time k';
      end

      function c = getColor(obj)
          c = 'white';
      end

      function t = getPriorCovTime(obj)
          t = 0;
          if (isfield(obj.timing, 'cov'))
              t = t + obj.timing.cov;
          end

          if (isfield(obj.timing, 'cond_cov'))
              t = t + obj.timing.cond_cov;
          end
      end

    end

    methods (Abstract)
      computeCov(obj)
      computeCondCov(obj)
    end

end

