function printTreeResults(parent)

for i = 1:parent.getChldrnNumber()
    child = parent.getChild(i);
    
    if (child.options.calcUnfoc || child.options.calcFoc)
        disp(child.getName());
    end
    
    if (child.options.calcFoc)
        disp('Focused Information: ');
        disp(child.focInfrmtn);
    end

    if (child.options.calcUnfoc)
        disp('Unfocused Information: ');
        disp(child.unfocInfrmtn);
    end

    if (~child.isLeaf)
        printTreeResults(child);
    end;

end;

end

