classdef BasicAction < BSPVertex
    
    properties
        parent
        A
        n
        N
        k
        m
        old
        new
        zeroVars
        invVars
        I_old
        I_old_dim
        Vold
        Vnew
        options
        focOldIndeces
        focOldIndeces_dim
        unfocOldIndeces
        unfocOldIndeces_dim
        focNewIndeces
        invUVars
        I_U_old
        newUVars
        U_new
        F_new
        cond_Vold
        cond_Vnew
        focInfrmtn
        focNewInfrmtn
        focOldInfrmtn
        unfocInfrmtn
        sumFocInfrmtn
        sumUnfocInfrmtn
    end
    
    methods
      function obj = BasicAction(P, A, options)
         obj = obj@BSPVertex();
         if nargin > 0
            obj.parent = P;
            P.addChild(obj);
            obj.A = A;
            obj.n = P.N;
            obj.N = size(A, 2);
            obj.k = obj.N - obj.n;
            obj.m = size(A, 1);
            obj.old = A(:, 1:obj.n);
            obj.new = A(:, obj.n+1:end);
            obj.zeroVars = find(sum(abs(obj.old)) == 0);
            obj.invVars = setdiff(1:obj.n, obj.zeroVars);
            obj.I_old = obj.old(:,obj.invVars);
            obj.I_old_dim = length(obj.invVars);
            obj.sumFocInfrmtn = NaN;
            obj.sumUnfocInfrmtn = NaN;
         end

         if nargin > 2
            obj.options = options;
            
            if ~isfield(obj.options, 'name')
                %obj.options.name = generateRndmString(5);
                
                currActionIndex = obj.getNextActionCounter();
                obj.options.name = num2str(currActionIndex, '%03.f');
            end
         end
      end

      function name = getName(obj)
          name = ['"', obj.options.name,'"'];
      end

      function setName(obj, name)
          obj.options.name = name;
      end

      function [priorIndeces, priorCondIndeces] = checkReqPriorCovIndeces(obj)
          
          % Analyze Focused Partitions
          if (obj.options.calcFoc)
              obj.focOldIndeces = obj.options.focIndeces(find(obj.options.focIndeces <= obj.n));
              obj.focOldIndeces_dim = length(obj.focOldIndeces);
              obj.unfocOldIndeces = setdiff([1:obj.n], obj.focOldIndeces);
              obj.unfocOldIndeces_dim = length(obj.unfocOldIndeces);
              
              obj.focNewIndeces = obj.options.focIndeces(find(obj.options.focIndeces > obj.n));
              
              if (isempty(obj.focOldIndeces) && isempty(obj.focNewIndeces))
                  warning('No variables are focused!!!');
              end
              
              if (~isempty(obj.focOldIndeces) && ~isempty(obj.focNewIndeces))
                  warning('Both old and new variables are focused!!! It''s currently not supported!');
              end

              obj.invUVars = setdiff(obj.invVars, obj.focOldIndeces);
              obj.I_U_old = obj.old(:,obj.invUVars);
              obj.newUVars = setdiff([(obj.n+1):obj.N], obj.focNewIndeces);
              obj.U_new = obj.new(:,obj.newUVars-obj.n);
              obj.F_new = obj.new(:,obj.focNewIndeces-obj.n);
              
              if ((obj.focOldIndeces_dim > 0) && ~isfield(obj.options, 'condCovMode'))
                  if (obj.focOldIndeces_dim > obj.unfocOldIndeces_dim)
                      obj.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
                  else
                      obj.options.condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
                  end
              end

          end

          % Collect indeces of posterior covariance (marginal and conditional)
          % required by children
          % (posterior = covariance at time after current action)
          reqPostIndeces = [];
          reqPostCondIndeces = [];
          for i = 1:obj.getChldrnNumber()
              child = obj.getChild(i);
              [currIndeces, currCondIndeces] = child.checkReqPriorCovIndeces();
              reqPostIndeces = union(reqPostIndeces, currIndeces);
              reqPostIndeces = reqPostIndeces(:)';
              reqPostCondIndeces = union(reqPostCondIndeces, currCondIndeces);
              reqPostCondIndeces = reqPostCondIndeces(:)';
          end

          % Calculate indeces of required prior covariance (marginal and conditional)
          % (prior = covariance at time of parent)
          priorIndeces = [];
          priorCondIndeces = [];
          
          %% Indeces required for children - Conditional Covariance
          obj.calcCondIndeces = reqPostCondIndeces;
          obj.cond_Vold = reqPostCondIndeces(find(reqPostCondIndeces <= obj.n));
          obj.cond_Vnew = setdiff(reqPostCondIndeces, obj.cond_Vold) - obj.n;

          % If we need to calculate conditional covariance
          if (~isempty(reqPostCondIndeces))
              if (isempty(obj.options.focIndeces))
                  warning('No variables are focused in the current action!!!');
                  return;
              end

              % Two ways to calculate 
              if (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_MARG) || (obj.focOldIndeces_dim == 0))
                  % Calculate posterior marginal of both conditional imdeces and
                  % focused indeces, following with schur complement
                  reqPostIndeces = union(reqPostIndeces, reqPostCondIndeces);
                  reqPostIndeces = reqPostIndeces(:)';
                  reqPostIndeces = union(reqPostIndeces, obj.options.focIndeces);
                  reqPostIndeces = reqPostIndeces(:)';
              %elseif (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_INFP))
              else
                  % Get prior conditional covariance and propagate it to
                  % the posterior time
                  priorCondIndeces = union(obj.cond_Vold, obj.invUVars)';
              end
          end

          % Prior conditional indeces required for calculating Focused IG/Entropy
          if (obj.options.calcFoc && ~isempty(obj.focOldIndeces))
              if (obj.needPriorCondCovForInvUVars())
                  priorCondIndeces = union(priorCondIndeces, obj.invUVars)';
              end              
          end

          % Save the list of required posterior marginal covariances
          obj.calcIndeces = reqPostIndeces;
          obj.Vold = reqPostIndeces(find(reqPostIndeces <= obj.n));
          obj.Vnew = setdiff(reqPostIndeces, obj.Vold) - obj.n;

          % Prior marginal required indeces
          priorIndeces = union(obj.Vold, obj.invVars)';
          
          % In case some inherited class needs extension here
          priorIndeces = union(priorIndeces, obj.additionalCovIndeces())';
      end

      function flag = needPriorCondCovForInvUVars(obj)
          flag = true;
      end

      function indeces = additionalCovIndeces(obj)
          indeces = [];
      end

      function propagate(obj)
          
          % Is this not last vertex
          if (~obj.isLeaf())
              obj.calcPosteriorCov();
              
              if (~isempty(obj.calcCondIndeces))
                  if (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_MARG) || (obj.focOldIndeces_dim == 0))
                      obj.calcPosteriorCondCovThroughMarginals();
                  elseif (strcmp(obj.options.condCovMode,TREE_CONSTANTS.COND_COV_MODE_INFP))
                      obj.calcPosteriorCondCovThroughPropagation();
                  end
              end;

              for i = 1:obj.getChldrnNumber()
                  child = obj.getChild(i);
                  child.propagate();
              end
          end
          
          % Perform additional calculations
          
          if (obj.options.calcUnfoc)
              obj.calcUnfocusedInformation();
          end

          if (obj.options.calcFoc)
              obj.calcFocusedInformation();
          end
      end

      function calcPosteriorCondCovThroughMarginals(obj)
          t = 0;
          
          % Calculate conditional covariance
          UF_cov = obj.getCov([obj.calcCondIndeces, obj.options.focIndeces]);
          inn = 1:length(obj.calcCondIndeces);
          foc = [1:length(obj.options.focIndeces)] + max(inn);
          F_marg_cov = UF_cov(foc,foc);
          U_marg_cov = UF_cov(inn,inn);
          UF_marg_cov = UF_cov(inn,foc);

          [R, t1] = calcQForm(F_marg_cov, UF_marg_cov);
          t = t + t1;

          tic
          cov_cond = U_marg_cov - R'*R;
          t = t + toc;

          obj.calcCondCov = cov_cond;
          obj.timing.cond_cov = t;
      end
      
    end

    methods (Abstract)
      calcPosteriorCov(obj)
      calcPosteriorCondCovThroughPropagation(obj)
      calcFocusedInformation(obj)
      calcUnfocusedInformation(obj)
    end
    
    methods (Static)
        
      function out=getNextActionCounter(init)
         persistent c;
         if (nargin > 0)
             c = init;
         else
             c = c + 1;
         end
         
         out = c; 
      end
      
    end

end

