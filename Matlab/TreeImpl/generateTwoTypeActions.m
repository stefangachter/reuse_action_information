function [ a1, a2 ] = generateTwoTypeActions(P, m, k )

J = zeros(m,P.N+k);
J(:,P.N+1:P.N+k) = rand(m, k);

% Selecting involved variables to be in number equal to 10% of dimension
%invNum = ceil(P.N/10);
invNum = 50;
invIndcs = randperm(P.N, invNum);
J(:,invIndcs) = rand(m, invNum);

options.calcUnfoc = false;
options.calcFoc = false;
options.focIndeces = [];
a1 = RectAction(P, J, options);

J1 = J(1:k,:);
J2 = J(k+1:end,:);

options.calcUnfoc = false;
options.calcFoc = false;
options.focIndeces = [];
a21 = SquarAction(P, J1, options);

options.calcUnfoc = false;
options.calcFoc = false;
options.focIndeces = [];
a22 = NAugAction(a21, J2, options);

a2 = a22;

end

