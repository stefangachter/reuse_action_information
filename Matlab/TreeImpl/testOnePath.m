%clear all;
close all;

rng(100);

n = 100;

% joint state
A_all = 0.1*rand(10*n,n);

% determine reordering so that eigenvalues appear sorted
[~, ~,P]=qr(A_all);
A_all = A_all*P;
info_mat = A_all'*A_all;
cov_mat = inv(info_mat);

% Init action counter
BasicAction.getNextActionCounter(0);


%cb = CovFormBelief(cov_mat);
%condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
options.condCovMode = condCovMode;
cb = InfFormBelief(info_mat, options);


a1 = generateNAugAction(cb, 100);
a11 = generateNAugAction(a1, 100);
a111 = generateNAugAction(a11, 100);

a2 = copyAction(a111, cb);
a21 = copyAction(a11, a2);
a211 = copyAction(a1, a21);

turnOnCalcUnfocOfTree(cb);
flatCopy = flatTreeCopy(cb);

cb.propagate();
flatCopy.propagate();

agregateInf(cb);
agregateInf(flatCopy);


if (~isInformtnIdentical(cb, flatCopy))
    warning('Two trees have different results!!!');
end


% printTreeResults(cb);
% printTreeResults(flatCopy);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
%subplot(2,1,1);
plotTree(cb, true);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plotTableOfActions(cb);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
%subplot(2,1,2);
plotTree(flatCopy, true);

Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plotTableOfActions(flatCopy);
