%clear all;
close all;

rng(100);

n = 100;

% joint state
A_all = 0.1*rand(10*n,n);

% determine reordering so that eigenvalues appear sorted
[~, ~,P]=qr(A_all);
A_all = A_all*P;
info_mat = A_all'*A_all;
cov_mat = inv(info_mat);

% Init action counter
BasicAction.getNextActionCounter(0);

%cb = CovFormBelief(cov_mat);
condCovMode = TREE_CONSTANTS.COND_COV_MODE_MARG;
%condCovMode = TREE_CONSTANTS.COND_COV_MODE_INFP;
options.condCovMode = condCovMode;
cb = InfFormBelief(info_mat, options);

a1 = generateRectAction(cb, 200, 100);
[a11, a12] = generateTwoTypeActions(a1, 500, 200);
%a11 = generateRectAction(a1, 200, 100);
a111 = generateSquarAction(a11, 100);
a111.options.calcFoc = true;
a111.options.focIndeces = (a111.N-20):(a111.N-10);
a121 = copyAction(a111, a12);

% a121 = generateSquarAction(a12, 100);
% a121.options.calcFoc = true;
% a121.options.focIndeces = (a121.N-20):(a121.N-10);
% 

a13 = generateSquarAction(a1, 100);
a13.options.calcFoc = true;
a13.options.focIndeces = (a13.N-5):(a13.N-3);

a2 = generateSquarAction(a1, 100);
a2.options.calcFoc = true;
a2.options.focIndeces = (a2.N-1):a2.N;
a2_empty = generateEmptyAction(a2);
a2_empty.options.calcFoc = true;
a2_empty.options.focIndeces = (a2_empty.N-1):a2_empty.N;
a3 = generateNAugAction(a2, 100);
a31 = generateRectAction(a3, 200, 100);
a31.options.calcFoc = true;
a31.options.focIndeces = (a31.N-1):a31.N;
a32 = generateRectAction(a3, 200, 100);
a32.options.calcFoc = true;
a32.options.focIndeces = (a32.N-1):a32.N;

a5 = generateRectAction(a2, 200, 100);
a6 = generateRectAction(a5, 200, 100);
a7 = generateRectAction(a6, 200, 100);
a7.options.calcFoc = true;
a7.options.focIndeces = (a7.N-1):a7.N;

a71 = generateNAugAction(a7, 100);
a71.options.calcFoc = true;
a71.options.focIndeces = (a71.N-1):a71.N;
a72 = generateNAugAction(a7, 100);
a72.options.calcFoc = true;
a72.options.focIndeces = (a72.N-1):a72.N;
a73 = generateNAugAction(a7, 100);
a73.options.calcFoc = true;
a73.options.focIndeces = (a73.N-1):a73.N;
a74 = generateNAugAction(a7, 100);
a74.options.calcFoc = true;
a74.options.focIndeces = (a74.N-1):a74.N;

changeCondCovModeOfTree(cb, condCovMode);

flatCopy = flatTreeCopy(cb);

cb.propagate();
flatCopy.propagate();

agregateInf(cb);
agregateInf(flatCopy);


if (~isInformtnIdentical({cb, flatCopy}))
    warning('Two trees have different results!!!');
end


% printTreeResults(cb);
% printTreeResults(flatCopy);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
%subplot(2,1,1);
plotTree(cb, true);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plotTableOfActions(cb);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
%subplot(2,1,2);
plotTree(flatCopy, true);


Hfig = figure();
set (Hfig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plotTableOfActions(flatCopy);
