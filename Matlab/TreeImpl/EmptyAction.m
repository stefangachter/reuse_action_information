classdef EmptyAction < BasicAction
    
    properties
    end
    
    methods
      function obj = EmptyAction(P, options)
          options.null = true;
          A = zeros(0, P.N);
          obj = obj@BasicAction(P, A, options);
      end
            
      function calcPosteriorCov(obj)
          obj.calcCov = [];
          obj.timing.cov = 0;
      end

      function calcPosteriorCondCovThroughPropagation(obj)
          obj.calcCondCov = [];
          obj.timing.cond_cov = 0;
      end

      function calcUnfocusedInformation(obj)
          obj.unfocInfrmtn = 0;
          obj.timing.infrmtn = 0;
      end

      function calcFocusedInformation(obj)
          t = 0;
          obj.focInfrmtn = 0;
          obj.focNewInfrmtn = 0;
          obj.focOldInfrmtn = 0;
          obj.timing.infrmtn = 0;
      end

      function flag = needPriorCondCovForInvUVars(obj)
          flag = false;
      end

      function c = getColor(obj)
          c = 'yellow';
      end

    end
    
end

